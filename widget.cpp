﻿#if defined(_MSC_VER) && (_MSC_VER >= 1600)
# pragma execution_character_set("utf-8")
#endif

#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    initState();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::initState()
{
    //初始化库
    initFFmpegLib();

    //界面设置
    this->setWindowTitle("FFmpeg");
    this->setObjectName("mainWidget");
    this->setStyleSheet("#mainWidget{background-color:gray}");

    ffmpegWidget = new FFmpegWidget();
    ui->gridLayout->addWidget(ffmpegWidget);
    connect(ffmpegWidget,&FFmpegWidget::sendPlayStart,this,&Widget::dealPlayStart);
    connect(ffmpegWidget,&FFmpegWidget::sendPlayFinish,this,&Widget::dealPlayFinish);
    connect(ffmpegWidget,&FFmpegWidget::sendPlayPosition,this,&Widget::dealPlayPosition);

    QStringList url;
    url << "E:/Video/谭晶-赤伶.mp4";
    url << "E:/Video/明月夜.mp4";
    url << "E:/学习资料/音视频相关/测试资源/谭晶-赤伶.mp4";
    url << "E:/学习资料/音视频相关/测试资源/2.mp4";
    url << "http://cctvalih5ca.v.myalicdn.com/live/cctv1_2/index.m3u8";
    url << "video=Integrated Camera";

    ui->cbBoxUrl->insertItems(0,url);
    ui->cbBoxUrl->setEditable(true);
    ui->cbBoxSpeed->setCurrentIndex(1);

    lastOpenDir = ui->cbBoxUrl->currentText();
}

void Widget::initFFmpegLib()
{
    FFmpegWidget::initFFmpegLib();

    QList<QCameraInfo> cameras = QCameraInfo::availableCameras();//获取当前可用摄像头
//    QString cam_name = QString("video=") + cameras.at(0).description();//格式必须为"video=Integrated Camera"
//    qDebug() << cameras.size()  << cam_name;
}


void Widget::on_btnOpen_clicked()
{
    ui->cbBoxSpeed->setCurrentIndex(1);
    QString text = ui->cbBoxUrl->currentText();
    ffmpegWidget->setPlayUrl(text);
    ffmpegWidget->open();
}

void Widget::on_btnClose_clicked()
{
    ffmpegWidget->close();
}

void Widget::on_btnPause_clicked()
{
    ffmpegWidget->pause();
}

void Widget::on_btnContinue_clicked()
{
    ffmpegWidget->next();
}

void Widget::on_sliderPlay_sliderPressed()
{
    ffmpegWidget->setSendPositionEnable(false);
}

void Widget::on_sliderPlay_sliderReleased()
{
    curSec = ui->sliderPlay->value();
    ui->sliderPlay->setValue(curSec);

    ffmpegWidget->setPlayPosition(curSec*1000);
    ffmpegWidget->setSendPositionEnable(true);
}

void Widget::on_cbBoxSpeed_currentIndexChanged(int index)
{
    ffmpegWidget->setPlaySpeed(index);
}

void Widget::dealPlayStart()
{
    int duration = ffmpegWidget->getLength();
    qDebug() << "开始播放" << "总时间" << duration;
    ui->sliderPlay->setRange(0,duration);
    ui->sliderPlay->setValue(0);
    curSec = 0;

    ffmpegWidget->setPlayAudioEnable(true);
}

void Widget::dealPlayFinish()
{
    qDebug() << "播放结束";
    curSec = 0;
    ui->sliderPlay->setValue(curSec);
}

void Widget::dealPlayPosition(int sec)
{
    curSec = sec;
    ui->sliderPlay->setValue(curSec);
//    qDebug() << "当前时间" << curSec;
}

void Widget::on_btnFile_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,"视频文件", lastOpenDir, "All Files (*)");
    QFileInfo info(fileName);
    lastOpenDir = info.absolutePath();

    if(!fileName.isEmpty())
    {
        ui->cbBoxUrl->addItem(fileName);
        ui->cbBoxUrl->setCurrentText(fileName);
    }
}

void Widget::on_radioBtnAudio_clicked(bool checked)
{
    ffmpegWidget->setPlayAudioEnable(checked);
}

void Widget::on_btnSaveFile_clicked()
{
    ffmpegWidget->startSaveFile("");
}

void Widget::on_btnStopSave_clicked()
{
    ffmpegWidget->stopSaveFile();
}

void Widget::on_btnCapture_clicked()
{
    ffmpegWidget->startCapture("");
}
