﻿#ifndef PROGRESSWAIT_H
#define PROGRESSWAIT_H

/************************************************************************
  *Copyright(C) ZXT 1174110975@qq.com
  *FileName: progresswait.h
  *Author: 周新童
  *Date: 2022-01-01
  *Description: 自定义控件实现的进度等待控件
*************************************************************************/

#include <QWidget>
#include <QPainter>
#include <QPaintEvent>
#include <QTimer>

class ProgressWait : public QWidget
{
    Q_OBJECT
public:
    explicit ProgressWait(QWidget *parent = nullptr);

    void initState();
    void startWait();
    void stopWait();

    void drawLine(QPainter *painter);

protected:
    void paintEvent(QPaintEvent *event);

signals:

public slots:
    void updateCurrentValue();

private:
    QTimer *timer;
    int currentValue;
};

#endif // PROGRESSWAIT_H
