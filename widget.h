﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTimer>
#include <QTime>
#include <QFileDialog>
#include "ffmpegwidget.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

    void initState();
    void initFFmpegLib();

private slots:
    void on_btnOpen_clicked();
    void on_btnClose_clicked();
    void on_btnPause_clicked();
    void on_btnContinue_clicked();
    void on_sliderPlay_sliderPressed();
    void on_sliderPlay_sliderReleased();
    void on_cbBoxSpeed_currentIndexChanged(int index);

    void dealPlayStart();
    void dealPlayFinish();
    void dealPlayPosition(int sec);

    void on_btnFile_clicked();
    void on_radioBtnAudio_clicked(bool checked);

    void on_btnSaveFile_clicked();
    void on_btnStopSave_clicked();
    void on_btnCapture_clicked();

private:
    Ui::Widget *ui;

    FFmpegWidget *ffmpegWidget;

    int totalSec;
    int curSec;
    QString lastOpenDir;
};
#endif // WIDGET_H
