HEADERS += \
    $$PWD/ffmpeghead.h \
    $$PWD/ffmpegsynclist.h \
    $$PWD/ffmpegthread.h \
    $$PWD/ffmpegwidget.h

FORMS += \
    $$PWD/ffmpegwidget.ui

SOURCES += \
    $$PWD/ffmpegsynclist.cpp \
    $$PWD/ffmpegthread.cpp \
    $$PWD/ffmpegwidget.cpp

