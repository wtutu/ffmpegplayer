﻿#ifndef FFMPEGWIDGET_H
#define FFMPEGWIDGET_H

/************************************************************************
  *Copyright(C) ZXT 1174110975@qq.com
  *FileName: ffmpegwidget.h
  *Author: 周新童
  *Date: 2022-01-11
  *Description: 视频显示类，内嵌ffmpeg播放的核心类，提供接口控制ffmpeg播放类的行为
*************************************************************************/

#include <QWidget>
#include <QPainter>
#include <QPaintEvent>
#include <QEvent>
#include <QPushButton>
#include <QBoxLayout>
#include <QPixmap>
#include <QScreen>
#include <QEventLoop>
#include "ffmpegthread.h"
#include "progresswait.h"

namespace Ui {
class FFmpegWidget;
}

enum PlayStatus
{
    Play_Default = 0,
    Play_Start,
    Play_Error,
    Play_Finish
};

class FFmpegWidget : public QWidget
{
    Q_OBJECT

public:
    explicit FFmpegWidget(QWidget *parent = nullptr);
    ~FFmpegWidget();

    static void initFFmpegLib();

    //状态初始化
    void initState();
    //透明信息层
    void initInfoWidget();

    //播放控制接口
    void setPlayUrl(const QString &url);
    void open();
    void close();
    void pause();
    void next();
    void clear();

    void msSleep(int msec);

    //读取信息、设置播放属性等
    int  getLength();
    void setPlayAudioEnable(bool enable);
    void setPlayPosition(qint64 position);
    void setSendPositionEnable(bool enable);
    int  getPlayStatus();
    void setPlaySpeed(int level);
    int  getVideoWidth();
    int  getVideoHeight();

    //录像、截屏
    void startCapture(const QString &path);
    void startSaveFile(const QString &path);
    void stopSaveFile();

    //渲染绘图
    void drawNote(QPainter *painter);
    void drawBorder(QPainter *painter);

protected:
    void paintEvent(QPaintEvent *);
    bool eventFilter(QObject *watched, QEvent *event);
    void resizeEvent(QResizeEvent *);
    void enterEvent(QEvent *);
    void leaveEvent(QEvent *);


    //信号槽部分主要完成各种状态信息的传递
signals:
    void sendPlayStart();             //可再向上层转发信号
    void sendPlayError();
    void sendPlayFinish();
    void sendPlayPosition(int sec);

public slots:
    void updateImage(const QImage &image);
    void threadPlayStart();
    void threadPlayError();
    void threadPlayFinish();
    void threadPlayPosition(int sec);

private:
    Ui::FFmpegWidget *ui;

    ProgressWait *progressWait;

    QWidget *infoWidget;
    QPushButton *btn1;
    QPushButton *btn2;

    FFmpegThread *thread;
    QImage image;

    int playStatus;    //0默认 1播放 2失败 3结束
    QString bgText;
    bool isSelect;
};

#endif // FFMPEGWIDGET_H
