QT       += core gui multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

#调试信息，用来产生dump文件调试
#QMAKE_CXXFLAGS_RELEASE = $$QMAKE_CFLAGS_RELEASE_WITH_DEBUGINFO
#QMAKE_LFLAGS_RELEASE = $$QMAKE_LFLAGS_RELEASE_WITH_DEBUGINFO
LIBS += -lDbgHelp


# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    progresswait.cpp \
    widget.cpp

HEADERS += \
    progresswait.h \
    widget.h

FORMS += \
    widget.ui

#子工程
INCLUDEPATH += $$PWD/ffmpeg
include ($$PWD/ffmpeg/ffmpeg.pri)

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

win32: LIBS += -L$$PWD/lib/ -lavcodec
win32: LIBS += -L$$PWD/lib/ -lavdevice
win32: LIBS += -L$$PWD/lib/ -lavfilter
win32: LIBS += -L$$PWD/lib/ -lavformat
win32: LIBS += -L$$PWD/lib/ -lavutil
win32: LIBS += -L$$PWD/lib/ -lpostproc
win32: LIBS += -L$$PWD/lib/ -lswresample
win32: LIBS += -L$$PWD/lib/ -lswscale

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include
